using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/imagens")]
public class ImagensController : ControllerBase
{
    [HttpGet("terreno/{nomeArquivo}")]
    public IActionResult ObterImagemTerreno(string nomeArquivo)
    {
        var caminhoCompleto = Path.Combine("Imagens/Terreno/", nomeArquivo);

        if (System.IO.File.Exists(caminhoCompleto))
        {
            var bytes = System.IO.File.ReadAllBytes(caminhoCompleto);
            return File(bytes, "image/jpg"); // ou o tipo de mídia apropriado para suas imagens
        }

        return NotFound();
    }

    [HttpGet("user/{nomeArquivo}")]
    public IActionResult ObterImagemUser(string nomeArquivo)
    {
        var caminhoCompleto = Path.Combine("Imagens/Profile/", nomeArquivo);

        if (System.IO.File.Exists(caminhoCompleto))
        {
            var bytes = System.IO.File.ReadAllBytes(caminhoCompleto);
            return File(bytes, "image/jpg"); // ou o tipo de mídia apropriado para suas imagens
        }

        return NotFound();
    }
}
