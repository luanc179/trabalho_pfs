using TerrenoApi.Models;
using TerrenoBackend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TerrenoBackend.Controllers;

// [Authorize]
[ApiController]
[Route("api/[controller]")]
public class TerrenoController : ControllerBase
{
    public TerrenoController(ApplicationDbContext db) =>
        this.db = db;

    // GET: api/Terreno
    [HttpGet]
    public ActionResult<IEnumerable<Terreno>> Get()
    {
        if (db.Terrenos == null)
            return NotFound();

        return db.Terrenos;
    }

    // GET: api/Terreno/5
    [HttpGet("{id}")]
    public ActionResult<Terreno> GetId(string id)
    {
        var obj = db.Terrenos.FirstOrDefault(x => x.Id == id);

        if (obj == null)
            return NotFound();

        return obj;
    }

    // POST: api/Terreno
    [HttpPost]
    public ActionResult<Terreno> Post(Terreno obj)
    {
        if (string.IsNullOrWhiteSpace(obj.Id))
            obj.Id = Guid.NewGuid().ToString();

        db.Terrenos.Add(obj);
        db.SaveChanges();

        return CreatedAtAction(
            nameof(GetId),
            new { id = obj.Id },
            obj
        );
    }

    // PUT: api/Terreno/5
    [HttpPut("{id}")]
    public IActionResult Put(string id, Terreno obj)
    {
        if (id != obj.Id)
            return BadRequest();

        db.Terrenos.Update(obj);
        db.SaveChanges();

        return NoContent();
    }



    // DELETE: api/Terreno/5
    [HttpDelete("{id}")]
    public IActionResult Delete(string id)
    {
        if (db.Terrenos == null)
            return NotFound();

        var obj = db.Terrenos.FirstOrDefault(x => x.Id == id);

        if (obj == null)
            return NotFound();

        db.Terrenos.Remove(obj);
        db.SaveChanges();

        return NoContent();
    }

    private readonly ApplicationDbContext db;
}
