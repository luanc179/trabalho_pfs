﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TerrenoBackend.Migrations
{
    /// <inheritdoc />
    public partial class AtualizadoTeste : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Peso",
                table: "Terrenos",
                newName: "Valor");

            migrationBuilder.RenameColumn(
                name: "Altura",
                table: "Terrenos",
                newName: "Metragem");

            migrationBuilder.AddColumn<int>(
                name: "Celular",
                table: "Terrenos",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Desponibilidade",
                table: "Terrenos",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Localizacao",
                table: "Terrenos",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Celular",
                table: "Terrenos");

            migrationBuilder.DropColumn(
                name: "Desponibilidade",
                table: "Terrenos");

            migrationBuilder.DropColumn(
                name: "Localizacao",
                table: "Terrenos");

            migrationBuilder.RenameColumn(
                name: "Valor",
                table: "Terrenos",
                newName: "Peso");

            migrationBuilder.RenameColumn(
                name: "Metragem",
                table: "Terrenos",
                newName: "Altura");
        }
    }
}
