﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TerrenoBackend.Migrations
{
    /// <inheritdoc />
    public partial class AtualizadoTeste4 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Desponibilidade",
                table: "Terrenos",
                newName: "Disponibilidade");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Disponibilidade",
                table: "Terrenos",
                newName: "Desponibilidade");
        }
    }
}
