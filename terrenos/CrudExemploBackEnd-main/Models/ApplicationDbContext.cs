using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using TerrenoBackend.Models;

namespace TerrenoApi.Models
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Terreno> Terrenos { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            options.UseSqlite($"Data Source={System.IO.Path.Join(path, "terreno.db")}");
        }
    }
}