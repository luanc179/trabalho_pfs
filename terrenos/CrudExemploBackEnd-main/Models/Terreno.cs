namespace TerrenoBackend.Models;
public class Terreno
{
    public string? Id { get; set; }
    public string Nome { get; set; } = "";
    public double Metragem { get; set; }
    public string Localizacao { get; set; } = "";
    public string Celular { get; set; } = "";
    public double Valor { get; set; }
    public string Disponibilidade { get; set; } = "DISPONIVEL";


}
