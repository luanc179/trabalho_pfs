import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Terreno from "./components/Terreno";
import Usuario from "./components/Usuario";
import Layout from "./components/Layout";
import Home from "./components/Home";
import Login from "./components/Login";
import Registrar from "./components/Registrar";
import App from "./App";

const Rotas = () => {
  return (<BrowserRouter>
    <Routes>
      <Route path='/' element={<Layout />}>
        <Route path='home' element={<Home />} />
        <Route index element={<Login />} />
        <Route path='login' element={<Login />}></Route>
        <Route path='registrar' element={<Registrar />}>
          <Route path=':acao' element={<Registrar />} />
        </Route>
        <Route path='terreno' element={<Terreno />}>
          <Route path=':acao' element={<Terreno />} />
          <Route path=':acao/:id' element={<Terreno />} />
        </Route>
        <Route path='usuario' element={<Usuario />}>
          <Route path=':acao' element={<Usuario />} />
          <Route path=':acao/:id' element={<Usuario />} />
        </Route>
        <Route path='inicio' element={<App />}></Route>
      </Route>
    </Routes>
  </BrowserRouter>);
};

export default Rotas;
