const CrudAcao = {
    listar: 'listar',
    consultar: 'consultar',
    inserir: 'inserir',
    alterar: 'alterar',
    excluir: 'excluir',
    voltar: 'voltar'
};

export default CrudAcao;
