import React, { useState, useEffect, useCallback } from "react";
import { Link } from "react-router-dom";
import CrudAcao from './CrudAcao';
import { apiAuthGet } from '../../apis';
import { useNavigate, useLocation } from "react-router-dom";
import '../../App.css';
import FiltroLista from "../FiltroLista";

const CrudLista = (props) => {
    const [objetos, setObjetos] = useState([]);
    const [carregando, setCarregando] = useState(true);
    const [filtroNome, setFiltroNome] = useState("");
    const [forceUpdate, setForceUpdate] = useState(true);

    const navigate = useNavigate();
    const location = useLocation();

    const navigateCallback = useCallback(navigate, [navigate]);

    useEffect(() => {
        apiAuthGet(props.entidade, (dados) => {
            const objetosFiltrados = filtroNome
                ? dados.filter(obj =>
                    Object.values(obj).some(
                        value =>
                            typeof value === 'string' &&
                            value.toLowerCase().includes(filtroNome.toLowerCase())
                    )
                )
                : dados;

            setObjetos(objetosFiltrados);
            setCarregando(false);
        }, (erro) => console.log(erro),
            navigateCallback, location.pathname);
    }, [carregando, filtroNome, props, navigateCallback, location, forceUpdate]);


    if (carregando) {
        return <div>Carregando...</div>;
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        setForceUpdate(prevState => !prevState);
    };


    const titulos = props.configCampos.titulos;
    const propriedades = props.configCampos.propriedades;
    return (
        <div className="table-list">
            <FiltroLista
                filtroNome={filtroNome}
                setFiltroNome={setFiltroNome}
                handleFormSubmit={handleFormSubmit}
            />
            <table className="table table-striped">
                <thead>
                    <tr>
                        {
                            titulos.map(x => <th scope="col">{x}</th>)
                        }
                        <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        objetos.map(obj => {
                            return (
                                <tr key={obj.id}>
                                    {
                                        propriedades.map(prop => {
                                            return <td key={prop}>{obj[prop]}</td>

                                        })
                                    }
                                    <td>
                                        <Link key={`consultar-${obj.id}`} to={`/${props.entidade}/${CrudAcao.consultar}/${obj.id}`} className="btn-list btn btn-secondary">Consultar</Link>
                                        <Link key={`alterar-${obj.id}`} to={`/${props.entidade}/${CrudAcao.alterar}/${obj.id}`} className="btn-list btn btn-warning">Alterar</Link>
                                        <Link key={`excluir-${obj.id}`} to={`/${props.entidade}/${CrudAcao.excluir}/${obj.id}`} className="btn-list btn btn-danger">Excluir</Link>
                                    </td>
                                </tr>
                            )
                        })
                    }

                </tbody>
            </table>
        </div>
    );
};

export default CrudLista;