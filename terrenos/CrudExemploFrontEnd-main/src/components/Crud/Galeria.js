import React, { useState, useEffect, useCallback } from "react";
import { Link } from "react-router-dom";
import CrudAcao from './CrudAcao';
import { apiAuthGet, apiAuthPutReservar } from '../../apis';
import { useNavigate, useLocation } from "react-router-dom";
import '../../App.css';
import FiltroLista from "../FiltroLista";

const Galeria = (props) => {
    const [objetos, setObjetos] = useState([]);
    const [carregando, setCarregando] = useState(true);
    const [filtroNome, setFiltroNome] = useState("");

    const navigate = useNavigate();
    const location = useLocation();

    const navigateCallback = useCallback(navigate, [navigate]);

    // Requisição do filtro
    useEffect(() => {
        apiAuthGet(props.entidade, (dados) => {
            const objetosFiltrados = filtroNome
                ? dados.filter(obj =>
                    Object.values(obj).some(
                        value =>
                            typeof value === 'string' &&
                            value.toLowerCase().includes(filtroNome.toLowerCase())
                    )
                )
                : dados;

            setObjetos(objetosFiltrados);
            setCarregando(false);
        }, (erro) => console.log(erro),
            navigateCallback, location.pathname);
    }, [carregando, filtroNome, props, navigateCallback, location]);

    // carrega listagem na execução da tela
    useEffect(() => {
        apiAuthGet(props.entidade, (dados) => {
            setObjetos(dados);
            setCarregando(false);
        }, (erro) => console.log(erro),
            navigate, location.pathname);
    }, [carregando, props, navigate, location]);

    const atualizarObjeto = (objetoAtualizado) => {
        setObjetos((objetos) => {
            return objetos.map((obj) => {
                if (obj.id === objetoAtualizado.id) {
                    return objetoAtualizado;
                }
                return obj;
            });
        });
    };

    const carregarImagem = (obj) => {
        return obj.imagemUrl = (`http://localhost:5016/api/imagens/terreno/terreno${Math.floor(Math.random() * 20) + 1}.jpg`);
    };


    if (carregando) {
        return <div>Carregando...</div>;
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
    };

    const reservarOuCancelarReserva = (obj) => {
        if (obj.disponibilidade === "DISPONIVEL") {
            // Reservar
            apiAuthPutReservar(`/${props.entidade}/${obj.id}`, obj)
                .then((response) => {
                    obj.id = obj.id;
                    obj.disponibilidade = "RESERVADO";
                    atualizarObjeto(obj);
                    alert("Reserva feita com sucesso");
                })
                .catch((error) => {
                    console.error("Erro ao reservar o item:", error);
                });
        } else if (obj.disponibilidade === "RESERVADO") {
            // Cancelar Reserva
            apiAuthPutReservar(`/${props.entidade}/${obj.id}`, obj)
                .then((response) => {
                    obj.id = obj.id;
                    obj.disponibilidade = "DISPONIVEL";
                    atualizarObjeto(obj);
                    alert("Reserva desfeita com sucesso")
                })
                .catch((error) => {
                    console.error("Erro ao cancelar a reserva do item:", error);
                });
        }
    };

    const titulos = props.configCampos.titulos;
    const propriedades = props.configCampos.propriedades;

    return (
        <> <FiltroLista
            filtroNome={filtroNome}
            setFiltroNome={setFiltroNome}
            handleFormSubmit={handleFormSubmit}
        />
            <div className="galeria-container">
                {objetos.map((obj) => (
                    <div key={obj.id} className="galeria-item">
                        <div className="galeria-imagem">
                            <img src={carregarImagem(obj)} alt={obj.titulo} />
                        </div>
                        <div className="galeria-detalhes">
                            {titulos.map((titulo, index) => (
                                <p key={index}>
                                    <strong>{titulo}:</strong> {obj[propriedades[index]]}
                                </p>
                            ))}
                        </div>
                        <div className="galeria-acoes">
                            <Link to={`/${props.entidade}/${CrudAcao.consultar}/${obj.id}`} className="btn-list btn btn-secondary">Consultar</Link>
                            <button
                                onClick={() => reservarOuCancelarReserva(obj)}
                                className={`btn btn-${obj.disponibilidade === "DISPONIVEL" ? "success" : "danger"}`}
                            >
                                {obj.disponibilidade === "DISPONIVEL" ? "Reservar" : "Cancelar Reserva"}
                            </button>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );
};

export default Galeria;
