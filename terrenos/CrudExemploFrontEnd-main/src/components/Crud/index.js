import React, { useState, useEffect } from "react";
import CrudAcao from "./CrudAcao";
import CrudLista from "./CrudLista";
import CrudManutencao from "./CrudManutencao";
import { Link } from "react-router-dom";
import '../../App.css';

const Crud = (props) => {
    const [acao, setAcao] = useState(props.acao);

    useEffect(() => {
        // Atualizar a ação quando props.acao mudar
        setAcao(props.acao);
    }, [props.acao]);

    const handleInserir = () => {
        // Lógica para inserir o objeto aqui...
        // Após a inserção bem-sucedida, atualizar a ação para listar
        setAcao(CrudAcao.listar);
    };

    if (!acao) {
        setAcao(CrudAcao.listar);
    }

    if (acao === CrudAcao.listar) {
        return (
            <div>
                <h1>Cadastro de {props.entidadeNomeAmigavelPlural}</h1>
                <Link className="btn btn-primary" to={`/${props.entidade}/${CrudAcao.inserir}`}>Inserir</Link>
                <br />
                <br />
                <CrudLista
                    entidade={props.entidade}
                    configCampos={props.configCampos}
                    objetos={props.objetos}
                />
            </div>
        );
    } else if (CrudAcao[acao] === undefined) {
        return <div>Ação inválida!</div>;
    } else {
        return <CrudManutencao
            entidade={props.entidade}
            entidadeNomeAmigavel={props.entidadeNomeAmigavel}
            acao={props.acao}
            novoObjeto={props.novoObjeto}
            id={props.id}
            campos={props.campos}
            onInserir={handleInserir}
        />;
    }
};

export default Crud;
