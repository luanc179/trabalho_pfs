import React from "react";

const FiltroLista = ({ filtroNome, setFiltroNome, handleFormSubmit }) => {
    return (
        <form onSubmit={handleFormSubmit}>
            <div className="mb-3">
                <label htmlFor="filtroNome" className="form-label">Filtrar:</label>
                <input
                    type="text"
                    className="form-control"
                    id="filtroNome"
                    value={filtroNome}
                    onChange={(e) => setFiltroNome(e.target.value)}
                />
            </div>
            {/* <button type="submit" className="btn btn-primary">Pesquisar</button> */}
        </form>
    );
};

export default FiltroLista;