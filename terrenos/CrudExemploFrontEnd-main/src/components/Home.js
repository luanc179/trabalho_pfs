import React from 'react';
import Galeria from './Crud/Galeria';
import "../App.css";
import { nomeUsuarioLogado } from '../auth';

const Home = () => {
    const entidade = "terreno";
    const configCampos = {
        titulos: ['Nome', 'Metragem', 'Localizacao', 'Celular', 'Valor', 'Disponibilidade'],
        propriedades: ['nome', 'metragem', 'localizacao', 'celular', 'valor', 'disponibilidade']
    };

    return (
        <div className="home-container">
            <h1>Bem- vindo {nomeUsuarioLogado()}</h1>
            <h2>Terrenos</h2>
            <Galeria entidade={entidade} configCampos={configCampos} />
        </div>
    );
};

export default Home;
