/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import { estaAutenticado, logout, nomeUsuarioLogado, usuarioPossuiPermissao } from "../auth";
import Logo from '../imgs/logo/logo.png';
import '../App.css';

const Layout = () => {
    const navigate = useNavigate();

    const sair = () => {
        logout();

        navigate('/');
    };

    let loginLogout = estaAutenticado() ? (
        <li className="nav-item" margin='2%'>
            <button className="nav-link btn" onClick={sair}>Logout</button>

        </li>
    ) : (
        <li className="nav-item">
            <a className="nav-link" href="/login">Login</a>
        </li>
    );

    const nomeUsuario = nomeUsuarioLogado();

    const usuarioIdentificacao = nomeUsuario ? <p className="mt-2">{nomeUsuario}</p> : null;

    const cadastrarAdmin = usuarioPossuiPermissao('Admin') ? (
        <a className="dropdown-item" href="/registrar/admin">Criar Usuário Admin</a>
    ) : null;

    const cadastrar = usuarioPossuiPermissao('Admin') ? (
        <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cadastros
            </a>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="/terreno">Terrenos</a>
                <a className="dropdown-item" href="/usuario">Usuarios</a>
                {cadastrarAdmin}
            </div>
        </li>
    ) : null;

    return (
        <React.Fragment>

            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div>
                    <div className='div_img_navbar'>
                        <a href="home"><img align='center' className="img_navbar" src={Logo} alt="logo" /></a>
                    </div>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                </div>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="home">Home</a>
                        </li>
                        {cadastrar}
                        {usuarioIdentificacao}
                        {loginLogout}
                    </ul>
                </div>
            </nav>
            <Outlet />
        </React.Fragment>
    );
};
export default Layout;
