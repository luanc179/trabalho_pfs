import React, { useState } from "react";
import { useParams } from "react-router-dom";
import Crud from "../Crud";
import '../../App.css';

const Terreno = () => {

    const { id, acao } = useParams();

    const configCampos = {
        titulos: ['Nome', 'Metragem', 'Localizacao', 'Celular', 'Valor', 'Disponibilidade'],
        propriedades: ['nome', 'metragem', 'localizacao', 'celular', 'valor', 'disponibilidade']
    }

    const [objetos, setObjetos] = useState([
        { id: '1', nome: 'Terreno1', metragem: '1000', localizacao: 'Centro', celular: '32 99999-9999', valor: 10.000, disponibilidade: 'DISPONIVEL' },
        { id: '2', nome: 'Terreno2', metragem: '2000', localizacao: 'Centro', celular: '32 88888-9999', valor: 20.000, disponibilidade: 'RESERVADO' },

    ]);

    const novoObjeto = () => {
        return { id: '', nome: '', metragem: '', localizacao: '', celular: '', valor: '', disponibilidade: 'DISPONIVEL' };

    };

    const retornarPorId = (id) => {
        return objetos.find(x => x.id === id);
    };

    const inserir = (obj) => {
        obj.id = obj.nome + obj.localizacao;
        setObjetos([...objetos, obj]);
    }

    const alterar = (obj) => {
        const novosObjetos = objetos.filter(x => x.id !== obj.id);
        setObjetos([...novosObjetos, obj]);
    }

    const excluir = (id) => {
        const novosObjetos = objetos.filter(x => x.id !== id);
        setObjetos([...novosObjetos]);
    }

    const campos = (somenteLeitura, obj, alterarCampo) => {
        return (<>
            <div className="form-group">
                <label htmlFor="nome">Nome</label>
                <input type="text" readOnly={somenteLeitura} value={obj.nome} onChange={(e) => alterarCampo(e.target.name, e.target.value)} className="form-control" id="nome" name="nome" />
            </div>
            <div className="form-group">
                <label htmlFor="metragem">Metragem</label>
                <input type="numeric" readOnly={somenteLeitura} value={obj.metragem} onChange={(e) => alterarCampo(e.target.name, e.target.value)} className="form-control" id="metragem" name="metragem" />
            </div>
            <div className="form-group">
                <label htmlFor="localizacao">Localização</label>
                <input type="text" readOnly={somenteLeitura} value={obj.localizacao} onChange={(e) => alterarCampo(e.target.name, e.target.value)} className="form-control" id="localizacao" name="localizacao" />
            </div>
            <div className="form-group">
                <label htmlFor="celular">Celular</label>
                <input type="nomeric" readOnly={somenteLeitura} value={obj.celular} onChange={(e) => alterarCampo(e.target.name, e.target.value)} className="form-control" id="celular" name="celular" />
            </div>
            <div className="form-group">
                <label htmlFor="valor">Valor</label>
                <input type="numeric" readOnly={somenteLeitura} value={obj.valor} onChange={(e) => alterarCampo(e.target.name, e.target.value)} className="form-control" id="valor" name="valor" />
            </div>
            <div className="form-group">
                <label htmlFor="disponibilidade">Disponibilidade</label>
                <input type="text" readOnly={somenteLeitura} value={obj.disponibilidade} onChange={(e) => alterarCampo(e.target.name, e.target.value)} className="form-control" id="disponibilidade" name="disponibilidade" />
            </div>
        </>);
    }

    return (
        <Crud
            entidade="terreno"
            entidadeNomeAmigavel="Terreno"
            entidadeNomeAmigavelPlural="Terreno"
            id={id}
            acao={acao}
            configCampos={configCampos}
            objetos={objetos}
            campos={campos}
            novoObjeto={novoObjeto}
            retornarPorId={retornarPorId}
            inserir={inserir}
            alterar={alterar}
            excluir={excluir}
        />
    );
};

export default Terreno;
