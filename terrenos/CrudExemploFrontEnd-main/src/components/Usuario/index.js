import React, { useState } from "react";
import { useParams } from "react-router-dom";
import Crud from "../Crud";
import '../../App.css';
const Usuario = () => {
    const { id, acao } = useParams();

    const configCampos = {
        titulos: ['E-mail'],
        propriedades: ['email']
    }

    const [objetos, setObjetos] = useState([
        // { id: '1', nome: 'Ana', email: 'ana@email.com' },
        // { id: '2', nome: 'Bruno', email: 'bruno@email.com' }
    ]);

    const novoObjeto = () => {
        return { id: '', email: '', password: '' };
    };

    const retornarPorId = (id) => {
        return objetos.find(x => x.id === id);
    };

    const inserir = (obj) => {
        obj.id = obj.email + obj.password;
        setObjetos([...objetos, obj]);
    }

    const alterar = (obj) => {
        const novosObjetos = objetos.filter(x => x.id !== obj.id);
        setObjetos([...novosObjetos, obj]);
    }

    const excluir = (id) => {
        const novosObjetos = objetos.filter(x => x.id !== id);
        setObjetos([...novosObjetos]);
    }

    const campos = (somenteLeitura, obj, alterarCampo) => {
        return (<>
            <div >
                <div className="form-group">
                    <label htmlFor="email">E-mail</label>
                    <input type="email" readOnly={somenteLeitura} value={obj.email} onChange={(e) => alterarCampo(e.target.name, e.target.value)} className="form-control" id="email" name="email" />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Senha</label>
                    <input type="password" readOnly={somenteLeitura} value={obj.password} onChange={(e) => alterarCampo(e.target.name, e.target.value)} className="form-control" id="password" name="password" />
                </div>
            </div>
        </>);

    }

    return (
        <Crud
            entidade="usuario"
            entidadeNomeAmigavel="Usuario"
            entidadeNomeAmigavelPlural="Usuarios"
            id={id}
            acao={acao}
            configCampos={configCampos}
            objetos={objetos}
            campos={campos}
            novoObjeto={novoObjeto}
            retornarPorId={retornarPorId}
            inserir={inserir}
            alterar={alterar}
            excluir={excluir}
        />
    );
};

export default Usuario;
